//
//  PIXViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXReelyActiveViewController.h"

#define UUID "7265656c-7941-6374-6976-652055554944"
#define RA1 "93208A27-8039-C75E-D4A1-4B565DD0B0A5"
#define RA2 "61987598-F402-E00E-BD5E-71421BDB9AE2"
#define RA3 "F3FA4093-892F-2FD5-D1F7-444EDD780E98"
#define RA4 "66A33E1A-D07A-FFA1-80C9-B63B390583DD"

@interface PIXReelyActiveViewController ()

-(void)updateRSSILabelWithKey:(NSString*)uuid withValue:(NSInteger)rssi;

@end

@implementation PIXReelyActiveViewController

@synthesize bluetoothManager;
@synthesize foundPeripheral;
@synthesize UUIDs;
@synthesize authorizedPeripherals;
@synthesize RSSIs;
@synthesize RSSIframesInTheZone;
@synthesize RSSIframesOutTheZone;
@synthesize RSSIupdates;
@synthesize RSSIStates;
@synthesize reelyActiveUUID;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // UUID de ReeelyActive
    reelyActiveUUID = [CBUUID UUIDWithString:[NSString stringWithUTF8String:UUID]];
    UUIDs = [[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInt:0],
                                                    [NSNumber numberWithInt:1],
                                                    [NSNumber numberWithInt:2],
                                                    [NSNumber numberWithInt:3]]
             forKeys:@[[NSString stringWithUTF8String:RA1],
                       [NSString stringWithUTF8String:RA2],
                       [NSString stringWithUTF8String:RA3],
                       [NSString stringWithUTF8String:RA4]]
             ];
    
    authorizedPeripherals = [[NSSet alloc] initWithObjects:
                             [NSString stringWithUTF8String:RA1],
                             [NSString stringWithUTF8String:RA2],
                             [NSString stringWithUTF8String:RA3],
                             [NSString stringWithUTF8String:RA4], nil];
    
    // UUIDs des périphériques bluetooth ReelyActive
    bluetoothManager =  [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];

    // Pour conserver les données
    RSSIs = [[NSMutableDictionary alloc] init];
    RSSIupdates = [[NSMutableDictionary alloc] init];
    
    RSSIframesOutTheZone = [[NSMutableDictionary alloc]
                           initWithObjects:@[[NSNumber numberWithInt:0],
                                             [NSNumber numberWithInt:0],
                                             [NSNumber numberWithInt:0],
                                             [NSNumber numberWithInt:0]]
                           forKeys:@[[NSString stringWithUTF8String:RA1],
                                     [NSString stringWithUTF8String:RA2],
                                     [NSString stringWithUTF8String:RA3],
                                     [NSString stringWithUTF8String:RA4]]
                           ];
    
    RSSIframesInTheZone = [[NSMutableDictionary alloc]
                  initWithObjects:@[[NSNumber numberWithInt:0],
                                    [NSNumber numberWithInt:0],
                                    [NSNumber numberWithInt:0],
                                    [NSNumber numberWithInt:0]]
                  forKeys:@[[NSString stringWithUTF8String:RA1],
                            [NSString stringWithUTF8String:RA2],
                            [NSString stringWithUTF8String:RA3],
                            [NSString stringWithUTF8String:RA4]]
                  ];
    
    RSSIStates = [[NSMutableDictionary alloc]
                initWithObjects:@[[NSNumber numberWithInteger:RSSI_DISCONNECTED],
                                  [NSNumber numberWithInteger:RSSI_DISCONNECTED],
                                  [NSNumber numberWithInteger:RSSI_DISCONNECTED],
                                  [NSNumber numberWithInteger:RSSI_DISCONNECTED]]
                forKeys:@[[NSString stringWithUTF8String:RA1],
                          [NSString stringWithUTF8String:RA2],
                          [NSString stringWithUTF8String:RA3],
                          [NSString stringWithUTF8String:RA4]]
                  ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CBCentralManager delegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"CBCentralManager changed state");
    if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CBCentralManager is powered on");
        NSDictionary *options = [[NSDictionary alloc]
                                 initWithObjects:@[[NSNumber numberWithBool:YES]]
                                 forKeys:@[CBCentralManagerScanOptionAllowDuplicatesKey]];
        [bluetoothManager scanForPeripheralsWithServices:@[reelyActiveUUID] options:options];
    }
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI
{
    
    // Fuck de données - on ignore ça
    if ([RSSI integerValue] > 0)
        return;
    
    foundPeripheral = peripheral;
    
    NSString *key = [[foundPeripheral identifier] UUIDString];
    if ([UUIDs objectForKey:key] == nil)
        return;
    
    // On vérifie l'état du périphérique
    // Si DISCONNECTED
    //      devient CONNECTED
    if ([[RSSIStates objectForKey:key] integerValue] == RSSI_DISCONNECTED) {
        [RSSIStates setObject:[NSNumber numberWithInteger:RSSI_CONNECTED] forKey:key];
    }
    
    // Mise-à-jour de l'heure de lecture
    
    // On regarde la nouvelle valeur RSSI
    // Si la valeur >= -70
    //      frameInTheZone ++
    //      Si frameInTheZone > 5
    //          devient PROXIMITY
    // Si la valeur < -70 && PROXIMITY
    //      frameOutTheZone ++
    //      Si frameOutTheZone > 5
    //          devient CONNECTED
    
    // On regarde la nouvelle valeur RSSI
    if ([RSSI integerValue] >= -70)
    {
        NSInteger framesIn = [[RSSIframesInTheZone objectForKey:key] integerValue];
        if (framesIn > 100) {
            // On est certain d'être prêt du périphérique
            [RSSIStates setObject:[NSNumber numberWithInteger:RSSI_PROXIMITY] forKey:key];
            [RSSIframesOutTheZone setObject:[NSNumber numberWithInt:0] forKey:key];
        }
        // Incrémente le buffer de frame
        framesIn++;
        [RSSIframesInTheZone setObject:[NSNumber numberWithInteger:framesIn] forKey:key];
    }
    else
    {
        NSInteger framesOut = [[RSSIframesInTheZone objectForKey:key] integerValue];
        if (framesOut > 100) {
            // On est certain d'être loin du périphérique
            [RSSIStates setObject:[NSNumber numberWithInteger:RSSI_CONNECTED] forKey:key];
            [RSSIframesInTheZone setObject:[NSNumber numberWithInt:0] forKey:key];
        }
        // Incrémente le buffer de frame
        framesOut++;
        [RSSIframesInTheZone setObject:[NSNumber numberWithInteger:framesOut] forKey:key];
    }
    
    
    // Mise à jour des label
    [self updateRSSILabelWithKey:key withValue:[RSSI integerValue]];
    
}

-(void)updateRSSILabelWithKey:(NSString*)uuid withValue:(NSInteger)rssi
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSInteger ID = [[UUIDs objectForKey:uuid] integerValue];
        NSInteger state = [[RSSIStates objectForKey:uuid] integerValue];
        
        
        UILabel *label = nil;
        if (ID == 0 ) {
            label = self.RSSILabel_1;
        } else if (ID==1) {
            label = self.RSSILabel_2;
        } else if (ID==2) {
            label = self.RSSILabel_3;
        } else if (ID==3) {
            label = self.RSSILabel_4;
        }
        
        label.text = [NSString stringWithFormat:@"%li",(long)rssi];
        if (state == RSSI_PROXIMITY) {
            label.textColor = [UIColor greenColor];
        } else if (state == RSSI_CONNECTED) {
            label.textColor = [UIColor blueColor];
        } else if (state == RSSI_DISCONNECTED) {
            label.textColor = [UIColor whiteColor];
        } else {
            label.textColor = [UIColor grayColor];
        }
    });
}



@end
