//
//  PIXHintImageViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIXHintImageViewController : UIViewController

@property NSTimer* hintTimer;

@property (strong, nonatomic) IBOutlet UIImageView *hintImageView;

@property NSInteger artworkID;
@property NSInteger hintID;

-(void)doneWithHint;

@end
