//
//  PIXArtworkViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PIXRABrowser.h"

@interface PIXArtworkViewController : UIViewController <PIXRABrowserDelegate>

typedef NS_ENUM(NSInteger, PIXHintTypes) {
    HINT_IMAGE,
    HINT_SOUND
};

@property (strong, nonatomic) IBOutlet UIImageView *backgroundSon;
@property (strong, nonatomic) IBOutlet UIImageView *hintVisuel;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundVisuel;
@property (strong, nonatomic) IBOutlet UIImageView *iconVisuel;
@property (strong, nonatomic) IBOutlet UIImageView *iconSound;
@property (strong, nonatomic) IBOutlet UIImageView *conclusionVisuel;

-(IBAction)unwindToCleanMediaAndToFinal:(UIStoryboardSegue *)segue;
-(IBAction)unwindToCleanMediaAndToNext:(UIStoryboardSegue *)segue;

- (IBAction)clickNext:(UIButton *)sender;

-(void)loadMediaForArtwork;
-(void)startArtwork;
-(void)goToNextHint;
-(void)goToConclusion;

-(void)didDismissContinue;

@property PIXRABrowser *bluetoothBrowser;

@property AVAudioPlayer *hintMusic;
@property AVAudioPlayer *conclusionMusic;

@property NSInteger artworkCount;
@property NSInteger hintCount;

@property NSInteger artworkID;
@property NSInteger hintID;

@end

// ------------------------------------
