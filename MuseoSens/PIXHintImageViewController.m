//
//  PIXHintImageViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXHintImageViewController.h"
#import "PIXArtworkViewController.h"

@interface PIXHintImageViewController ()

@end

@implementation PIXHintImageViewController

@synthesize artworkID;
@synthesize hintID;
@synthesize hintTimer;
@synthesize hintImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *filename = [NSString stringWithFormat:@"art%i_hint%i.png", artworkID+1, hintID+1];
    
    hintImageView.image = [UIImage imageNamed:filename];
    
    /*
    hintTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:self
                                   selector:@selector(hintTimeIsOver:)
                                   userInfo:nil
                                    repeats:NO];
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Timer

-(void) hintTimeIsOver:(NSTimer *)timer
{
    [self doneWithHint];
}

#pragma mark - Navigation

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)doneWithHint {
    /*
    PIXArtworkViewController *artViewController = (PIXArtworkViewController*)[self presentingViewController];
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [artViewController didDismissHint];
                             }
     ];
     */
}

@end
