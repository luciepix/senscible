//
//  PIXHintSoundViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXHintSoundViewController.h"
#import "PIXArtworkViewController.h"

@interface PIXHintSoundViewController ()

@end

@implementation PIXHintSoundViewController

@synthesize artworkID;
@synthesize hintID;
@synthesize hintTimer;

@synthesize hintMusicPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*
    hintTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                 target:self
                                               selector:@selector(hintTimeIsOver:)
                                               userInfo:nil
                                                repeats:NO];
     */
    
    NSString *filename = [NSString stringWithFormat:@"art%i_hint%i", artworkID+1, hintID+1];
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"aif"];
    
    NSError *error;
    self.hintMusicPlayer = [[AVAudioPlayer alloc]
                                  initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
    [self.hintMusicPlayer prepareToPlay];
    [self.hintMusicPlayer play];
    self.hintMusicPlayer.volume = 0.0;
    
    [self doVolumeFadeIn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doVolumeFadeIn {
  
    if (hintMusicPlayer.volume < 0.99) {
        hintMusicPlayer.volume = hintMusicPlayer.volume + 0.01;
        [self performSelector:@selector(doVolumeFadeIn) withObject:nil afterDelay:0.1];
    }
}

-(void)doVolumeFadeOut
{
    if (hintMusicPlayer.volume > 0.1) {
        hintMusicPlayer.volume = hintMusicPlayer.volume - 0.05;
        [self performSelector:@selector(doVolumeFadeOut) withObject:nil afterDelay:0.1];
        NSLog(@"vol:%f", hintMusicPlayer.volume);
    } else {
        // Stop and get the sound ready for playing again
        [hintMusicPlayer stop];
        hintMusicPlayer.currentTime = 0;
        [hintMusicPlayer prepareToPlay];
        hintMusicPlayer.volume = 1.0;
        [self doneWithHint];
    }
}

#pragma mark - Timer

-(void) hintTimeIsOver:(NSTimer *)timer
{
    [self doVolumeFadeOut];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */

- (void)doneWithHint {
    
    //[self doVolumeFadeOut];
    
    PIXArtworkViewController *artViewController = (PIXArtworkViewController*)[self presentingViewController];
    /*
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [artViewController didDismissHint];
                             }
     ];
     */
}


@end
