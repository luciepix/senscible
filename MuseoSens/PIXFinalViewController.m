//
//  PIXFinalViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXFinalViewController.h"

@interface PIXFinalViewController ()

@end

@implementation PIXFinalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*
    [NSTimer scheduledTimerWithTimeInterval:10.0
                                     target:self
                                   selector:@selector(timeIsOver:)
                                   userInfo:nil
                                    repeats:NO];
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
-(void) timeIsOver:(NSTimer*)timer
{
    [self performSegueWithIdentifier:@"unwindToCleanMediaAndToFinal" sender:self];
    //[self performSegueWithIdentifier:@"toBeginning" sender:self];
}
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickDone:(id)sender {
    [self performSegueWithIdentifier:@"unwindToCleanMediaAndToFinal" sender:self];
}
@end
