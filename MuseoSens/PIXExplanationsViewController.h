//
//  PIXExplanationsViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIXExplanationsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *animatedView1;
@property (strong, nonatomic) IBOutlet UIImageView *animatedView2;
@property (strong, nonatomic) IBOutlet UIImageView *animatedView3;
@property (strong, nonatomic) IBOutlet UIImageView *animatedViewFinal;

-(void)animate1;
-(void)animate2;
-(void)animate3;
-(void)animateFinal;

@end
