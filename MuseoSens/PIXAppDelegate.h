//
//  PIXAppDelegate.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
