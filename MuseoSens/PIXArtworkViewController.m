//
//  PIXArtworkViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXArtworkViewController.h"
#import "PIXHintImageViewController.h"
#import "PIXHintSoundViewController.h"
#import "PIXConclusionViewController.h"

@interface PIXArtwork: NSObject
@property NSInteger index;
@property NSString *name;
@property NSString *device;
@property NSArray *hintTypes;
@end

@implementation PIXArtwork
@synthesize index;
@synthesize name;
@synthesize device;
@synthesize hintTypes;

- (id)initWithIndex:(NSInteger)idx {
    self = [super init];
    if (self) {
        index = idx;
    }
    return self;
}
@end

// ----------------------------------------------

@interface PIXArtworkViewController ()
@property NSArray *artworks;
-(void)prepareArtworks;
@end

@implementation PIXArtworkViewController

@synthesize bluetoothBrowser;

@synthesize artworkCount;
@synthesize artworkID;
@synthesize hintCount;
@synthesize hintID;
@synthesize artworks;

@synthesize backgroundSon;
@synthesize backgroundVisuel;
@synthesize hintVisuel;
@synthesize iconSound;
@synthesize iconVisuel;
@synthesize conclusionVisuel;

@synthesize hintMusic;
@synthesize conclusionMusic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    bluetoothBrowser = [[PIXRABrowser alloc] init];
    [bluetoothBrowser setDelegate:self];
    
    artworkCount = 4;
    hintCount = 2;
    
    artworkID = 0;
    hintID = 0;
    
    // Creation du tableau d'oeuvres
    artworks = [[NSArray alloc] initWithObjects:
                [[PIXArtwork alloc] initWithIndex:1],
                [[PIXArtwork alloc] initWithIndex:2],
                [[PIXArtwork alloc] initWithIndex:3],
                [[PIXArtwork alloc] initWithIndex:4], nil];
    
    [self prepareArtworks];
    
    backgroundSon.alpha = 0.0;
    backgroundVisuel.alpha = 0.0;
    hintVisuel.alpha = 0.0;
    iconVisuel.alpha = 0.0;
    iconSound.alpha = 0.0;
    conclusionVisuel.alpha = 0.0;
    
    [self loadMediaForArtwork];
    [self startArtwork];
    
}

-(void)prepareArtworks
{
    // Informations sur chaque oeuvre
    // Le Masque autochtone
    PIXArtwork *artwork = [artworks objectAtIndex:0];
    artwork.name = @"Masque";
    artwork.device = @"93208A27-8039-C75E-D4A1-4B565DD0B0A5";
    artwork.hintTypes = [[NSArray alloc] initWithObjects:
                         [NSNumber numberWithInteger:HINT_SOUND],
                         [NSNumber numberWithInteger:HINT_IMAGE], nil];
    
    // Les objets religieux
    artwork = [artworks objectAtIndex:1];
    artwork.name = @"Religion";
    artwork.device = @"66A33E1A-D07A-FFA1-80C9-B63B390583DD";
    artwork.hintTypes = @[[NSNumber numberWithInteger:HINT_IMAGE],
                          [NSNumber numberWithInteger:HINT_SOUND]];
    
    // L'orage / L'ours
    artwork = [artworks objectAtIndex:2];
    artwork.name = @"Orage";
    artwork.device = @"61987598-F402-E00E-BD5E-71421BDB9AE2";
    artwork.hintTypes = @[[NSNumber numberWithInteger:HINT_SOUND],
                          [NSNumber numberWithInteger:HINT_IMAGE]];
    
    // Le port
    artwork = [artworks objectAtIndex:3];
    artwork.name = @"Orage";
    artwork.device = @"F3FA4093-892F-2FD5-D1F7-444EDD780E98";
    artwork.hintTypes = @[[NSNumber numberWithInteger:HINT_IMAGE],
                          [NSNumber numberWithInteger:HINT_SOUND]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickNext:(UIButton *)sender {
    [self goToNextHint];
}

-(void)loadMediaForArtwork
{
    PIXArtwork *artwork = [artworks objectAtIndex:artworkID];
    
    //NSInteger tmpID = 0;
    
    //while (tmpID < hintCount-1) {
        if ([[artwork.hintTypes objectAtIndex:hintID] integerValue] == HINT_IMAGE) {
            // Indice visuel
            NSString *imgname = [NSString stringWithFormat:@"art%i_hint%i.png", artworkID+1, hintID+1];
            hintVisuel.image = [UIImage imageNamed:imgname];
            NSLog(@"Loaded image hint: %@", imgname);
        } else {
            // Indice sonore
            NSString *filename = [NSString stringWithFormat:@"art%i_hint%i", artworkID+1, hintID+1];
            NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"aif"];
            NSError *error;
            self.hintMusic = [[AVAudioPlayer alloc]
                                    initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
            [self.hintMusic prepareToPlay];
            //[self.hintMusic play];
            NSLog(@"Loaded sound hint: %@", filename);
        }
    //}
    //else
    //{
        NSString *imgname = [NSString stringWithFormat:@"art%i_conclusion.png", artworkID+1];
        conclusionVisuel.image = [UIImage imageNamed:imgname];
        conclusionVisuel.alpha = 0.0;
        NSLog(@"Loaded image conclusion: %@", imgname);
        
        // Conclusion sonore
        NSString *filename = [NSString stringWithFormat:@"art%i_conclusion", artworkID+1];
        NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"aif"];
        NSError *error;
        self.conclusionMusic = [[AVAudioPlayer alloc]
                                initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
        [self.conclusionMusic prepareToPlay];
        //[self.hintMusic play];
        self.conclusionMusic.volume = 0.0;
        
        NSLog(@"Loaded music conclusion: %@", filename);
        
        //return;
    //}
    
}

-(void)startArtwork
{
    backgroundVisuel.alpha = 0.0;
    hintVisuel.alpha = 0.0;
    iconVisuel.alpha = 0.0;
    iconSound.alpha = 0.0;
    backgroundSon.alpha = 0.0;
    conclusionVisuel.alpha = 0.0;
    
    PIXArtwork *artwork = [artworks objectAtIndex:artworkID];
    [bluetoothBrowser searchForDevice:artwork.device];
    
    if ([[artwork.hintTypes objectAtIndex:hintID] integerValue] == HINT_IMAGE)
        [self animateToVisualHint];
    else
        [self animateToSoundHint];
}

-(void)goToNextHint
{
    // Il nous reste des indices à visiter
    if (hintID < hintCount-1)
    {
        hintID++; // le prochain indice
        PIXArtwork *artwork = [artworks objectAtIndex:artworkID];
        
        [bluetoothBrowser searchForDevice:artwork.device];
        [self loadMediaForArtwork];
        
        if ([[artwork.hintTypes objectAtIndex:hintID] integerValue] == HINT_IMAGE)
            [self animateToVisualHint];
        else
            [self animateToSoundHint];
    }
    // On a visité tous les indices
    else
    {
        // donc on a terminé l'oeuvre d'art
        [self goToConclusion];
    }
}

-(void)goToConclusion
{
    [self animateToConclusion];
    [bluetoothBrowser stopSearching];
}

-(void)animateToVisualHint
{
    hintVisuel.alpha = 0.0;
    
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         backgroundVisuel.alpha = 1.0;
                         hintVisuel.alpha = 1.0;     // move
                         iconVisuel.alpha = 1.0;
                         iconSound.alpha = 0.0;
                         backgroundSon.alpha = 0.0;
                         conclusionVisuel.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)animateToSoundHint
{
    self.hintMusic.volume = 0.0;
    [hintMusic play];
    [self doHintVolumeFadeIn];
    
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         backgroundVisuel.alpha = 0.0;
                         //hintVisuel.alpha = 0.0;     // move
                         iconVisuel.alpha = 0.0;
                         iconSound.alpha = 1.0;
                         backgroundSon.alpha = 1.0;
                         conclusionVisuel.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)animateToConclusion
{
    conclusionVisuel.alpha = 0.0;
    
    self.conclusionMusic.volume = 0.0;
    [conclusionMusic play];
    [self doConclusionVolumeFadeIn];
    
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         backgroundVisuel.alpha = 0.0;
                         hintVisuel.alpha = 0.0;     // move
                         iconVisuel.alpha = 0.0;
                         iconSound.alpha = 0.0;
                         backgroundSon.alpha = 0.0;
                         conclusionVisuel.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                     }];
    
    [NSTimer scheduledTimerWithTimeInterval:15.0
                                     target:self
                                   selector:@selector(conclusionIsDone:)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)conclusionIsDone:(NSTimer*)timer {
    
    [self doHintVolumeFadeOut];
    
    // Il nous reste des oeuvres d'art à explorer
    if (artworkID < artworkCount-1)
    {
        [self performSegueWithIdentifier:@"toContinue" sender:self];
    }
    // On a terminé l'experience
    else
    {
        [self performSegueWithIdentifier:@"toFinalCompleted" sender:self];
    }
}

-(void)doHintVolumeFadeIn {
    
    if (hintMusic.volume < 0.99) {
        hintMusic.volume = hintMusic.volume + 0.01;
        [self performSelector:@selector(doHintVolumeFadeIn) withObject:nil afterDelay:0.1];
    }
}

-(void)doHintVolumeFadeOut
{
    if (hintMusic.volume > 0.1) {
        hintMusic.volume = hintMusic.volume - 0.05;
        [self performSelector:@selector(doHintVolumeFadeOut) withObject:nil afterDelay:0.1];
        NSLog(@"vol:%f", hintMusic.volume);
    } else {
        // Stop and get the sound ready for playing again
        [hintMusic stop];
        hintMusic.currentTime = 0;
        [hintMusic prepareToPlay];
        hintMusic.volume = 1.0;
    }
}

-(void)doConclusionVolumeFadeIn {
    
    if (conclusionMusic.volume < 0.99) {
        conclusionMusic.volume = conclusionMusic.volume + 0.01;
        [self performSelector:@selector(doConclusionVolumeFadeIn) withObject:nil afterDelay:0.1];
    }
}

-(void)doConclusionVolumeFadeOut
{
    if (conclusionMusic.volume > 0.05) {
        conclusionMusic.volume = conclusionMusic.volume - 0.05;
        [self performSelector:@selector(doConclusionVolumeFadeOut) withObject:nil afterDelay:0.1];
        NSLog(@"vol:%f", hintMusic.volume);
    } else {
        // Stop and get the sound ready for playing again
        [conclusionMusic stop];
        conclusionMusic.currentTime = 0;
        [conclusionMusic prepareToPlay];
        conclusionMusic.volume = 1.0;
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"toContinue"]) {
        backgroundVisuel.alpha = 0.0;
        hintVisuel.alpha = 0.0;
        iconVisuel.alpha = 0.0;
        iconSound.alpha = 0.0;
        backgroundSon.alpha = 0.0;
        conclusionVisuel.alpha = 0.0;
    }
}

-(void)didDismissContinue
{
    [self doConclusionVolumeFadeOut];
    
    // On recommence au premier indice
    hintID = 0;
    artworkID++;
    
    PIXArtwork *firstArtwork = [artworks objectAtIndex:artworkID];
    [bluetoothBrowser searchForDevice:firstArtwork.device];
    
    [NSTimer scheduledTimerWithTimeInterval:4.0
                                     target:self
                                   selector:@selector(okToGo:)
                                   userInfo:nil
                                    repeats:NO];
    /*
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         backgroundVisuel.alpha = 0.0;
                         hintVisuel.alpha = 0.0;     // move
                         iconVisuel.alpha = 0.0;
                         iconSound.alpha = 0.0;
                         backgroundSon.alpha = 0.0;
                         conclusionVisuel.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self loadMediaForArtwork];
                         [self startArtwork];
                     }];
     */
}

-(void)okToGo:(NSTimer*)timer
{
    [self loadMediaForArtwork];
    [self startArtwork];
    
}

-(IBAction)unwindToCleanMediaAndToNext:(UIStoryboardSegue *)segue
{
    /*
    [self doConclusionVolumeFadeOut];
    [self doHintVolumeFadeOut];
    
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         backgroundVisuel.alpha = 0.0;
                         hintVisuel.alpha = 0.0;     // move
                         iconVisuel.alpha = 0.0;
                         iconSound.alpha = 0.0;
                         backgroundSon.alpha = 0.0;
                         conclusionVisuel.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
     */
}


-(IBAction)unwindToCleanMediaAndToFinal:(UIStoryboardSegue *)segue
{
    
    [self doConclusionVolumeFadeOut];
    [self doHintVolumeFadeOut];
    
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         backgroundVisuel.alpha = 0.0;
                         hintVisuel.alpha = 0.0;     // move
                         iconVisuel.alpha = 0.0;
                         iconSound.alpha = 0.0;
                         backgroundSon.alpha = 0.0;
                         conclusionVisuel.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                     }];
}

#pragma mark - PIXRABrowserDelegate methods

-(void)deviceFoundInTheArea
{
    [self goToNextHint];
}

-(void)deviceFoundInProximity
{
    [self goToConclusion];
}

@end
