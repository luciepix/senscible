//
//  PIXHintSoundViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PIXHintSoundViewController : UIViewController

@property NSTimer* hintTimer;

@property NSInteger artworkID;
@property NSInteger hintID;

@property AVAudioPlayer *hintMusicPlayer;

-(void)doneWithHint;

@end
