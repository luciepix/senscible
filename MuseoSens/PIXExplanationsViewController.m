//
//  PIXExplanationsViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXExplanationsViewController.h"

@interface PIXExplanationsViewController ()
@end

@implementation PIXExplanationsViewController

@synthesize animatedView1;
@synthesize animatedView2;
@synthesize animatedView3;
@synthesize animatedViewFinal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    animatedView1.alpha = 1.0;
    [self animate1];
}

-(void)animate1
{
    [UIView animateWithDuration:3.0 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         animatedView1.alpha = 0.0;    // move
                     }
                     completion:^(BOOL finished){
                         [self animate2];
                     }];
}

-(void)animate2
{
    [UIView animateWithDuration:3.0 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         animatedView2.alpha = 1.0;    // move
                     }
                     completion:^(BOOL finished){
                         [self animate3];
                     }];
}

-(void)animate3
{
    [UIView animateWithDuration:3.0 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         //animatedView2.alpha = 0.0;
                         animatedView3.alpha = 1.0;     // move
                     }
                     completion:^(BOOL finished){
                         [self animateFinal];
                     }];
}

-(void)animateFinal
{
    [UIView animateWithDuration:3.0 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         animatedViewFinal.alpha = 1.0;
                         //animatedView3.alpha = 0.0;     // move
                     }
                     completion:^(BOOL finished){
                         [NSTimer scheduledTimerWithTimeInterval:3.0
                                                          target:self
                                                        selector:@selector(timeIsOver:)
                                                        userInfo:nil
                                                         repeats:NO];
                     }];
}

#pragma mark - Timer

-(void) timeIsOver:(NSTimer *)timer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"toArtworks" sender:self];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
