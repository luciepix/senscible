//
//  PIXBreathViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PIXConclusionViewController : UIViewController

@property NSInteger artworkID;

@property AVAudioPlayer *musicPlayer;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

-(void)doneWithConclusion;

@end
