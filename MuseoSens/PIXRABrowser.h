//
//  PIXRABrowser.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-09.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

//Define the protocol for the delegate
@protocol PIXRABrowserDelegate
- (void)deviceFoundInTheArea;
- (void)deviceFoundInProximity;
@end

@interface PIXRABrowser : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, assign) id  <PIXRABrowserDelegate> delegate;

@property CBCentralManager *bluetoothManager;
@property CBUUID *reelyActiveUUID;
@property NSArray *devices;

@property NSString *searchingDevice;
@property NSInteger deviceID;
@property BOOL deviceFound;
@property BOOL deviceClose;

@property NSMutableArray *pollProximityTime;
@property NSMutableArray *pollAreaTime;

-(void)resetSearch;
-(void)searchForDevice:(NSString*)UUID;
-(void)stopSearching;

@end
