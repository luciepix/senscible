//
//  PIXViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface PIXReelyActiveViewController : UIViewController <CBCentralManagerDelegate, CBPeripheralDelegate>

typedef NS_ENUM(NSInteger, RSSIState) {
    RSSI_DISCONNECTED,
    RSSI_CONNECTED,
    RSSI_PROXIMITY
};

// Bluetooth machins
@property NSDictionary *UUIDs;
@property CBCentralManager *bluetoothManager;
@property CBUUID *reelyActiveUUID;

@property (strong) CBPeripheral *foundPeripheral;
@property NSSet *authorizedPeripherals;
@property NSMutableDictionary *RSSIs;
@property NSMutableDictionary *RSSIframesInTheZone;
@property NSMutableDictionary *RSSIframesOutTheZone;
@property NSMutableDictionary *RSSIupdates;
@property NSMutableDictionary *RSSIStates;

@property (strong, nonatomic) IBOutlet UILabel *RSSILabel_1;
@property (strong, nonatomic) IBOutlet UILabel *RSSILabel_2;
@property (strong, nonatomic) IBOutlet UILabel *RSSILabel_3;
@property (strong, nonatomic) IBOutlet UILabel *RSSILabel_4;


@end
