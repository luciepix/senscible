//
//  PIXRABrowser.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-09.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXRABrowser.h"

#define RAUUID "7265656c-7941-6374-6976-652055554944"

@implementation PIXRABrowser

@synthesize delegate;

@synthesize devices;
@synthesize bluetoothManager;
@synthesize reelyActiveUUID;
@synthesize searchingDevice;
@synthesize deviceID;
@synthesize deviceClose;
@synthesize deviceFound;

@synthesize pollProximityTime;
@synthesize pollAreaTime;

- (id)init {
    self = [super init];
    if (self) {
        // Any custom setup work goes here
        devices = [[NSArray alloc] initWithObjects:
            @"93208A27-8039-C75E-D4A1-4B565DD0B0A5",
            @"61987598-F402-E00E-BD5E-71421BDB9AE2",
            @"F3FA4093-892F-2FD5-D1F7-444EDD780E98",
            @"66A33E1A-D07A-FFA1-80C9-B63B390583DD", nil];
        
        pollAreaTime = [[NSMutableArray alloc] init];
        pollProximityTime = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)searchForDevice:(NSString *)UUID
{
    if (searchingDevice && [UUID caseInsensitiveCompare:searchingDevice] == 0)
        return;
    
    // UUID de ReeelyActive
    if (!reelyActiveUUID)
        reelyActiveUUID = [CBUUID UUIDWithString:[NSString stringWithUTF8String:RAUUID]];
    
    // UUIDs des périphériques bluetooth ReelyActive
    if (!bluetoothManager)
        bluetoothManager =  [[CBCentralManager alloc]initWithDelegate:self queue:nil options:nil];
    
    if ([bluetoothManager state] == CBCentralManagerStatePoweredOn) {
        //NSLog(@"CBCentralManager is powered on");
        NSDictionary *options = [[NSDictionary alloc]
                                 initWithObjects:@[[NSNumber numberWithBool:YES]]
                                 forKeys:@[CBCentralManagerScanOptionAllowDuplicatesKey]];
        [bluetoothManager scanForPeripheralsWithServices:@[reelyActiveUUID] options:options];
    }
    
    [self resetSearch];
    
    for (NSString *devuuid in devices) {
        if ([devuuid caseInsensitiveCompare:UUID] == 0) {
            deviceID = [devices indexOfObject:devuuid];
            break;
        }
    }
    
    if (deviceID < 0)
    {
        NSLog(@"Device does not exist");
        return;
    }
    
    searchingDevice = UUID;
}

-(void)stopSearching
{
    [self resetSearch];
    [bluetoothManager stopScan];
}

-(void)resetSearch
{
    searchingDevice = nil;
    deviceID = -1;
    deviceFound = NO;
    deviceClose = NO;
    
    [pollAreaTime removeAllObjects];
    [pollProximityTime removeAllObjects];
}

#pragma mark - CBCentralManager delegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"CBCentralManager changed state");
    if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CBCentralManager is powered on");
        NSDictionary *options = [[NSDictionary alloc]
                                 initWithObjects:@[[NSNumber numberWithBool:YES]]
                                 forKeys:@[CBCentralManagerScanOptionAllowDuplicatesKey]];
        [bluetoothManager scanForPeripheralsWithServices:@[reelyActiveUUID] options:options];
    }
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI
{
    
    if ([[[peripheral identifier] UUIDString] caseInsensitiveCompare:searchingDevice] != 0)
        return;
    
    // Fuck de données - on ignore ça
    if ([RSSI integerValue] > 0)
        return;
    
    if ([RSSI intValue] > -75) {
        NSLog(@"[%i] %i", deviceID, [RSSI integerValue]);
    
        [pollProximityTime addObject:[NSDate date]];
        
        /*
         CFTimeInterval startTime = CACurrentMediaTime();
         // perform some action
         CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
         */
        
        NSMutableArray *garbage = [[NSMutableArray alloc] init];
        
        for (NSDate* time in pollProximityTime) {
            NSTimeInterval timeInterval = [time timeIntervalSinceNow];
            
            if (timeInterval < -2.0) {
                [garbage addObject:pollProximityTime];
            }
        }
        
        for (NSDate *todelete in garbage) {
            [pollProximityTime removeObject:todelete];
        }
        [garbage removeAllObjects];
        
        if ([pollProximityTime count] > 10 && !deviceClose) {
            NSLog(@"++++++++++");
            deviceClose = YES;
            deviceFound = YES;
            // delegate --> CLOSE PROXIMITY
            [[self delegate] deviceFoundInProximity];
        }
    }
    else if ([RSSI intValue] > -87)
    {
        NSLog(@"[%i] %i", deviceID, [RSSI integerValue]);
        
        [pollAreaTime addObject:[NSDate date]];
        
        NSMutableArray *garbage = [[NSMutableArray alloc] init];
        
        for (NSDate* time in pollAreaTime) {
            NSTimeInterval timeInterval = [time timeIntervalSinceNow];
            
            if (timeInterval < -2.0) {
                [garbage addObject:pollAreaTime];
            }
        }
        
        for (NSDate *todelete in garbage) {
            [pollAreaTime removeObject:todelete];
        }
        [garbage removeAllObjects];
        
        if ([pollAreaTime count] > 8 && !deviceFound) {
            NSLog(@"-------");
            deviceClose = NO;
            deviceFound = YES;
            // delegate --> IN THE AREA
            [[self delegate] deviceFoundInTheArea];
        }
    }
}


@end
