//
//  PIXContinueViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIXContinueViewController : UIViewController

- (void)doneWithContinue;

- (IBAction)clickStop:(UIButton *)sender;
- (IBAction)clickContinue:(UIButton *)sender;

@end
