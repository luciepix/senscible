//
//  PIXBreathViewController.m
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-08.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import "PIXConclusionViewController.h"
#import "PIXArtworkViewController.h"

@interface PIXConclusionViewController ()

@end

@implementation PIXConclusionViewController

@synthesize imageView;
@synthesize musicPlayer;
@synthesize artworkID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Chronomètre temporaire
    [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:self
                                   selector:@selector(timeIsOver:)
                                   userInfo:nil
                                    repeats:NO];
    
    NSString *imgname = [NSString stringWithFormat:@"art%i_conclusion.png", artworkID+1];
    imageView.image = [UIImage imageNamed:imgname];
    
    NSString *filename = [NSString stringWithFormat:@"art%i_conclusion", artworkID+1];
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"aif"];
    
    NSError *error;
    self.musicPlayer = [[AVAudioPlayer alloc]
                            initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
    [self.musicPlayer prepareToPlay];
    [self.musicPlayer play];
    self.musicPlayer.volume = 0.0;
    
    [self doVolumeFadeIn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doVolumeFadeIn {
    
    if (musicPlayer.volume < 0.99) {
        musicPlayer.volume = musicPlayer.volume + 0.01;
        [self performSelector:@selector(doVolumeFadeIn) withObject:nil afterDelay:0.1];
    }
}

-(void)doVolumeFadeOut
{
    if (musicPlayer.volume > 0.1) {
        musicPlayer.volume = musicPlayer.volume - 0.05;
        [self performSelector:@selector(doVolumeFadeOut) withObject:nil afterDelay:0.1];
        NSLog(@"vol:%f", musicPlayer.volume);
    } else {
        // Stop and get the sound ready for playing again
        [musicPlayer stop];
        musicPlayer.currentTime = 0;
        [musicPlayer prepareToPlay];
        musicPlayer.volume = 1.0;
        [self doneWithConclusion];
    }
}

#pragma mark - Timer

-(void) timeIsOver:(NSTimer *)timer
{
    [self doVolumeFadeOut];
}

#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)doneWithConclusion {
    
    PIXArtworkViewController *artViewController = (PIXArtworkViewController*)[self presentingViewController];
    /*
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [artViewController didDismissConclusion];
                             }
     ];
     */
}

@end
