//
//  PIXWelcomeViewController.h
//  MuseoSens
//
//  Created by Lucie Bélanger on 2014-11-09.
//  Copyright (c) 2014 Pixiole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIXWelcomeViewController : UIViewController

@end
